# file  -- engine.py --

from modules.story import *


class Engine(object):

    def __init__(self, story):
        self.story = story

    def play(self):
        current_scene = self.story.first_scene()
        last_scene = self.story.last_scene()

        next_scene_name, player = current_scene.enter()

        while current_scene != last_scene:
            current_scene = self.story.next_scene(next_scene_name)
            next_scene_name, player = current_scene.enter(player)
