# file  -- fight.py --

from modules.guns import *
from modules.chars import *


class Fight(object):

    def __init__(self, player, mob0, mob1, go):
        self.player = player
        self.mobs = [mob0]

        if mob1 == None:
            ghost = Ghost()
            self.mobs.append(ghost)
        else:
            self.mobs.append(mob1)

        self.go = go

    def commence(self):
        print()
        prev = ['move','move','shoot']
        win = False

        # Repeating action rounds
        while not win:
            busy = False
            up = 2
            up_text = 0

            # Player's action
            while not busy:
                print("What will", self.player.name, "do?")
                action = input("> ").lower()

                if action == 'help':
                    print((up + up_text) * "\033[F\033[2K" + "\033[F")
                    print(self.player.name, "can:")
                    print("- Shoot        ", end=" ")
                    print("- Move in      ")
                    print("- Reload       ", end=" ")
                    print("- Move out     ", end=" ")
                    print("- Show stats   ")
                    print("- Swap guns    ", end=" ")
                    print("- Dodge        ")
                    print()
                    up = 2
                    up_text = 5

                elif action == 'shoot':
                    busy = True
                    prev[2] = 'shoot'

                    # Two targets
                    if self.mobs[0].alive() and self.mobs[1].alive():
                        hit = False
                        print(up * "\033[F\033[2K" + "\033[F")
                        up = 2

                        # One target must be hit
                        while hit != True:
                            print("Which one to shoot; left or right?")
                            target = input("> ").lower()

                            if target == 'left':
                                aim = 0
                                hit = True
                            elif target == 'right':
                                aim = 1
                                hit = True
                            else:
                                print(up * "\033[F\033[2K" + "\033[F")
                                print("That is not a valid target.")
                                up = 3
                        
                    # One target
                    elif self.mobs[1].alive():
                        aim = 1
                    elif self.mobs[0].alive():
                        aim = 0
                    else:
                        exit("Error: Trying to shoot at two dead mobs.")

                    # Damage the target
                    print("\033[1J\033[H")
                    print(self.player.name, "shoots at", end=" ")
                    print(self.mobs[aim].name.lower() + ".")
                    dmg = self.player.shoot()[self.mobs[aim].dist]
                    self.mobs[aim].hp -= dmg

                    if dmg > 0:
                        print(self.mobs[aim].name, "takes a hit!")

                    if not self.mobs[aim].alive():
                        self.mobs[aim].die()

                    # Win if all enemies are dead
                    if not self.mobs[0].alive() and not self.mobs[1].alive():
                        win = True

                elif action == 'reload':
                    busy = True
                    prev[2] = 'reload'
                    print("\033[1J\033[H")
                    self.player.reload()

                elif action == 'swap guns':
                    print((up + up_text) * "\033[F\033[2K" + "\033[F")
                    print(self.player.name, "has a:")
                    items = 1

                    for tag in self.player.pack:
                        print("-", tag.title())
                        items += 1

                    print()
                    print("Which gun will", self.player.name, "equip?")
                    equip = input("> ").lower()

                    if not self.player.pack.get(equip, False):
                        print((3 + items) * "\033[F\033[2K" + "\033[F")
                        print(self.player.name, "does not have that gun.")
                        up = 3
                        up_text = 0
                    else:
                        print((3 + items) * "\033[F\033[2K" + "\033[F")
                        self.player.gun = self.player.pack[equip]
                        print(self.player.name, "equipped the", equip + "!")
                        print()
                        up = 2
                        up_text = 2

                elif action == 'move in':
                    busy = True
                    prev[2] = 'move in'

                    for i in [0,1]:
                        self.mobs[i].dist = 'close'

                    print("\033[1J\033[H")
                    print(self.player.name, "moves in!")

                elif action == 'move out':
                    busy = True
                    prev[2] = 'move out'

                    for i in [0,1]:
                        self.mobs[i].dist = 'far'

                    print("\033[1J\033[H")
                    print(self.player.name, "moves out!")

                elif action == 'dodge':
                    busy = True
                    print("\033[1J\033[H")
                    print(self.player.name, "dodges out of the way.")

                elif action == 'show stats':
                    print((up + up_text) * "\033[F\033[2K" + "\033[F")
                    self.player.inspect()
                    print()
                    up = 2
                    up_text = 3

                else:
                    print(up * "\033[F\033[2K" + "\033[F")
                    print("That is not a valid command. Enter 'Help' for help.")
                    up = 3

            # Enemy's actions
            for i in [0,1]:

                if self.mobs[i].alive():

                    if prev[i] != 'move' and self.mobs[i].dist != self.go:
                        self.mobs[i].move()
                        prev[i] = 'move'

                    else:

                        if self.mobs[i].gun.ammo > 0:
                            print(self.mobs[i].name, "shoots at", end=" ")
                            print(self.player.name + ".")
                            dmg = self.mobs[i].shoot()[self.mobs[i].dist]

                            if action == 'dodge' and prev[2] == 'dodge':
                                print(self.mobs[i].name, end=" ")
                                print("saw another dodge coming!")
                                print(self.player.name, "takes a hit!")
                            elif action == 'dodge':
                                dmg = 0
                                print(self.player.name, "dodged it!")
                            else:
                                print(self.player.name, "takes a hit!")

                            self.player.hp -= dmg
                            prev[i] = 'shoot'

                        else:
                            self.mobs[i].reload()
                            prev[i] = 'move'

                    if not self.player.alive():
                        self.player.die()
                        return False

            if action == 'dodge':
                prev[2] = 'dodge'

            print()

        # The fight has been won
        print("Cleared the area.")
        self.player.reload()
        return True
