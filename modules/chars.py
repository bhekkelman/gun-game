# file  -- chars.py --

from modules.guns import *


class Character(object):

    def __init__(self, name, hp, gun, dist):
        self.name = name
        self.hp = hp
        self.gun = gun
        self.dist = dist

    def alive(self):
        if self.hp > 0:
            return True
        else:
            return False

    def die(self):
        if self.alive():
            self.hp = 0

        print(self.name, "was killed!")

    def inspect(self):
        if self.alive():
            print(self.name + "'s remaining hp is {}.".format(self.hp))
            if self.gun != None:
                print(self.name, "has {} ammo left.".format(self.gun.ammo))
            else:
                print(self.name, "doesn't have a gun.")
        else:
            print(self.name, "is dead.")

    def move(self):
        if self.alive():
            if self.dist == 'far':
                self.dist = 'close'
                print(self.name, "moves in!")
            elif self.dist == 'close':
                self.dist = 'far'
                print(self.name, "moves out!")

    def shoot(self):
        if self.alive():
            if self.gun != None:
                return self.gun.shoot()
            else:
                print(self.name, "doesn't have a gun!")

    def reload(self):
        if self.alive():
            if self.gun != None:
                self.gun.reload()
                print(self.name, "reloaded!")
            else:
                print(self.name, "doesn't have a gun!")


class Player(Character):

    def __init__(self, name):
        super(Player, self).__init__(name, 30, None, None)
        self.pack = {}
        self.keys = {}

    def move(self):
        pass


class Mob(Character):

    def __init__(self, name, dist):
        super(Mob, self).__init__(name, 6, Lazergun(), dist)


class Boss(Character):

    def __init__(self, name, dist):
        super(Boss, self).__init__(name, 13, Plasmagun(), dist)


class Ghost(Character):

    def __init__(self):
        self.name = ""
        self.hp = 0
        self.gun = None
        self.dist = 'close'

    def alive(self):
        return False

    def die(self):
        pass

    def inspect(self):
        pass

    def move(self):
        pass

    def shoot(self):
        pass

    def reload(self):
        pass
