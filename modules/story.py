# file  -- story.py --

from modules.guns import *
from modules.chars import *
from modules.fight import *


text_file = open('./modules/text.txt')
text_content = text_file.read()
text_file.close()
text_lines = text_content.split('\n\n\n')
text_cue = {}
for i in range(0, int(len(text_lines)/2)):
    text_cue[text_lines[2*i]] = text_lines[2*i + 1]


class Scene(object):

    def enter(self, player):
        print("This scene hasn't been developed yet.")

        return 'outro', player


class Death(Scene):

    def enter(self, player):
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        print(player.name, "died a horrible death on an alien ship. R.I.P.")
        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        exit("--- GAME OVER ---\n")


class Intro(Scene):

    def enter(self):
        print("\033[1J\033[H")
        print(text_cue['introduction_1'])
        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['introduction_2'])
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        named = False
        up = 2

        while not named:
            print("What is your name?")
            name = input("> ").title()

            if not name.isalpha():
                print(up * "\033[F\033[2K" + "\033[F")
                print("A proper name contains only letters. You should know.")
            elif len(name) > 7:
                print(up * "\033[F\033[2K" + "\033[F")
                print("That name is too long. Pick a nickname or something.")
            elif len(name) < 3:
                print(up * "\033[F\033[2K" + "\033[F")
                print("That name is a little short. Use your imagination.")
            else:
                player = Player(name)
                named = True

            up = 3

        print("\nOk", player.name + ", it is time to shine.")
        input("\n> [Press Enter to continue.] ")

        return 'armory', player


class Armory(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['armory_1'].format(player.name))
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        print(text_cue['armory_2'].format(player.name))
        print()
        picked = False
        up = 2

        while not picked:
            print("Which gun will", player.name, "pick?")
            pick = input("> ").lower()

            if pick == 'handgun':
                player.pack[pick] = Handgun()
                picked = True
            elif pick == 'shotgun':
                player.pack[pick] = Shotgun()
                picked = True
            elif pick == 'machinegun':
                player.pack[pick] = Machinegun()
                picked = True
            else:
                print(up * "\033[F\033[2K" + "\033[F")
                print("That gun is not available.")
                up = 3

        player.gun = player.pack[pick]
        print("\n" + player.name, "took the", pick + "!")
        print("Time to head out.")
        input("\n> [Press Enter to continue.] ")

        return 'docking_bay', player


class DockingBay(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['docking_bay'].format(player.name))
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        answered = False
        up = 2

        while not answered:
            print("Will", player.name, "shoot, or yield?")
            answer = input("> ").lower()

            if answer == 'shoot':
                print("\033[1J\033[H")
                print(text_cue['docking_bay_shoot'].format(player.name))
                answered = True
            elif answer == 'yield':
                print("\033[1J\033[H")
                print(text_cue['docking_bay_yield'].format(player.name))
                return 'death', player
            else:
                print(up * "\033[F\033[2K" + "\033[F")
                print("That is not an appropriate action here.")
                up = 3

        input("\n> [Press Enter to continue.] ")

        return 'hallway', player


class Hallway(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['hallway_1'].format(player.name))
        alien_1 = Mob('The alien', 'close')
        fight_1 = Fight(player, alien_1, None, 'close')
        outcome_1 = fight_1.commence()

        if not outcome_1:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['hallway_2'].format(player.name))
        alien_2 = Mob('The alien', 'far')
        fight_2 = Fight(player, alien_2, None, 'far')
        outcome_2 = fight_2.commence()

        if not outcome_2:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['hallway_3'].format(player.name))
        player.pack['lazergun'] = Lazergun()
        print("\n[", player.name, "picked up a lazergun. ]")
        input("\n> [Press Enter to continue.] ")
        print("\033[1J")
        print(text_cue['bridge_intro'].format(player.name))

        return 'bridge', player


class Bridge(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['bridge_1'].format(player.name))
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        print(text_cue['bridge_2'].format(player.name))
        print()
        choice = False
        up = 2

        while not choice:
            print("Which room will", player.name, "enter?")
            room = input("> ").lower()
            wrongroom = True

            for c in ['red', 'green', 'blue']:

                if room == c + ' room' or room == c:

                    if c in player.keys:
                        print(up * "\033[F\033[2K" + "\033[F")
                        print(player.name, "has already cleared the", c, "room.")
                        wrongroom = False
                    else:
                        return c + '_room', player

            if room == 'command room' or room == 'command':

                if 'red' not in player.keys:
                    pass
                elif 'green' not in player.keys:
                    pass
                elif 'blue' not in player.keys:
                    pass
                else:
                    return 'command_room', player

                print(up * "\033[F\033[2K" + "\033[F")
                print("The command room door is locked.")
                wrongroom = False

            if wrongroom:
                print(up * "\033[F\033[2K" + "\033[F")
                print("That is not one of the rooms", player.name, "can enter.")

            up = 3


class RedRoom(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['red_room_1'].format(player.name))
        alien_1 = Mob('The left alien', 'close')
        alien_2 = Mob('The right alien', 'far')
        fight = Fight(player, alien_1, alien_2, 'close')
        outcome = fight.commence()

        if not outcome:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        player.keys['red'] = True
        print(text_cue['red_room_2'].format(player.name))
        input("\n> [Press Enter to continue.] ")

        return 'bridge', player


class GreenRoom(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['green_room_1'].format(player.name))
        alien_1 = Mob('The left alien', 'far')
        alien_2 = Mob('The right alien', 'close')
        fight = Fight(player, alien_1, alien_2, 'switch')
        outcome = fight.commence()

        if not outcome:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        player.keys['green'] = True
        print(text_cue['green_room_2'].format(player.name))
        input("\n> [Press Enter to continue.] ")

        return 'bridge', player


class BlueRoom(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['blue_room_1'].format(player.name))
        alien_1 = Mob('The left alien', 'far')
        alien_2 = Mob('The right alien', 'close')
        fight = Fight(player, alien_1, alien_2, 'far')
        outcome = fight.commence()

        if not outcome:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        player.keys['blue'] = True
        print(text_cue['blue_room_2'].format(player.name))
        input("\n> [Press Enter to continue.] ")

        return 'bridge', player


class CommandRoom(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['command_room_1'].format(player.name))
        boss = Boss('The big alien', 'close')
        fight = Fight(player, boss, None, 'switch')
        outcome = fight.commence()

        if not outcome:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['command_room_2'].format(player.name))
        player.pack['plasmagun'] = Plasmagun()
        print("\n[", player.name, "picked up a plasmagun. ]")
        input("\n> [Press Enter to continue.] ")

        return 'escape', player


class Escape(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['escape_1'].format(player.name))
        alien_1 = Mob('The left alien', 'close')
        alien_2 = Mob('The right alien', 'close')
        fight_1 = Fight(player, alien_1, alien_2, 'close')
        outcome_1 = fight_1.commence()

        if not outcome_1:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['escape_2'].format(player.name))
        alien_3 = Mob('The left alien', 'far')
        alien_4 = Mob('The right alien', 'far')
        fight_2 = Fight(player, alien_3, alien_4, 'far')
        outcome_2 = fight_2.commence()

        if not outcome_2:
            return 'death', player

        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        print(text_cue['escape_3'].format(player.name))

        return 'outro', player


class Outro(Scene):

    def enter(self, player):
        print("\033[1J\033[H")
        print(text_cue['outro_1'].format(player.name))
        input("\n> [Press Enter to continue.] ")
        print("\033[F\033[2K\033[F")
        print(text_cue['outro_2'].format(player.name))
        input("\n> [Press Enter to continue.] ")
        print("\033[1J\033[H")
        exit("--- YOU WIN ---\n")

        return '', player


class Story(object):

    scenes = {
        'death': Death(),
        'intro': Intro(),
        'armory': Armory(),
        'docking_bay': DockingBay(),
        'hallway': Hallway(),
        'bridge': Bridge(),
        'red_room': RedRoom(),
        'green_room': GreenRoom(),
        'blue_room': BlueRoom(),
        'command_room': CommandRoom(),
        'escape': Escape(),
        'outro': Outro()
        }

    def next_scene(self, scene_name):
        return Story.scenes[scene_name]

    def first_scene(self):
        return self.next_scene('intro')

    def last_scene(self):
        return self.next_scene('outro')
