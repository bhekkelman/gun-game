# file  -- guns.py --

class Gun(object):

    def __init__(self, ammo, shots, damage_c, damage_f, sound):
        self.ammo = ammo
        self.maxammo = ammo
        self.shots = shots
        self.damage_c = damage_c
        self.damage_f = damage_f
        self.sound = sound

    def shoot(self):
        if self.ammo > 0:
            print(self.sound)
            self.ammo -= self.shots
            return {'close': self.damage_c, 'far': self.damage_f}
        else:
            print("*Click click*")
            print("Out of ammo!")
            return {'close': 0, 'far': 0}

    def reload(self):
        self.ammo = self.maxammo


class Handgun(Gun):

    def __init__(self):
        super(Handgun, self).__init__(10, 2, 2, 2, "*Peng peng*")


class Shotgun(Gun):

    def __init__(self):
        super(Shotgun, self).__init__(2, 2, 6, 1, "*Blam blam*")


class Machinegun(Gun):

    def __init__(self):
        super(Machinegun, self).__init__(15, 5, 3, 2, "*Ra-ta-ta-ta-tat*")


class Lazergun(Gun):

    def __init__(self):
        super(Lazergun, self).__init__(6, 2, 2, 1, "*Pew pew*")


class Plasmagun(Gun):

    def __init__(self):
        super(Plasmagun, self).__init__(2, 1, 5, 3, "*Bzeeem*")
