# file  -- game.py --

from modules.story import *
from modules.engine import *


the_story = Story()
the_game = Engine(the_story)
the_game.play()
